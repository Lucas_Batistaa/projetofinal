package Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TesteDatabase {
	
	public static void main(String[] args) {
	
		Usuario usuario = new Usuario("4", "cleber5", "5@5", "123345", "1");
		usuario.save();
		
		try {

			ResultSet rs = usuario.listall();
			
			while ( rs.next() ){
				System.out.print(
					"\n"+
					rs.getString("idUsuario") + " | " +
					rs.getString("nome") + " | " +
					rs.getString("email") + " | " +
					rs.getString("senha") + " | " +
					rs.getString("nivel") 
				);
			
		}
		
		} catch (SQLException e) {
			System.out.print("Verifique o comando ou a dependencia de chave extrangeira!");
		}
		
	}
	
	

}
