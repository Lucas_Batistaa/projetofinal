package View;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import Model.Usuario;


public class FrmListUsuarios extends JFrame{

	private Usuario usuario = null;
	

	
	
	private String[][] resultSetToArray( ResultSet rs ) {
		
		try {
			
			int qtdColumns = rs.getMetaData().getColumnCount();
			int qtdLines   = 0;
			
			rs.beforeFirst();
			while ( rs.next() ){
				qtdLines  += 1; 
			}
			
			
			String[][] tmp_dados = new String[qtdLines][qtdColumns];
			rs.beforeFirst();
			for (int lin = 0; lin < qtdLines; lin++) {
				rs.next();
				for (int col = 0; col < qtdColumns; col++) {
					tmp_dados[lin][col] = rs.getString(col+1);
				}	
			}
			return (tmp_dados);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}

	public FrmListUsuarios() {
		this.setTitle("Cadastro do Usuario");
		this.setBounds(250, 250, 600, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		String[] cabecalho = {"idUsuario","nome","email","senha","nivel"};
		String[][] dados   =  this.resultSetToArray(new Usuario().listall());
		JTable jTable = new JTable(dados,cabecalho);
		jTable.setBounds(5, 5, 500, 300);
		
		jTable.addMouseListener( new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {}
			@Override public void mousePressed(MouseEvent e) {}
			@Override public void mouseExited(MouseEvent e) {}
			@Override public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {
				// JOptionPane.showMessageDialog(null, ((String)jTable.getValueAt(jTable.getSelectedRow(), jTable.getSelectedColumn())));
				// JOptionPane.showMessageDialog(null, (jTable.getValueAt( jTable.getSelectedRow() , 0)));
				switch (e.getButton()) {
					case 1: JOptionPane.showMessageDialog(null, "Botão Esquerdo Pressionado!"); break;
					case 2: JOptionPane.showMessageDialog(null, "Botão Central Pressionado!"); break;
					case 3: JOptionPane.showMessageDialog(null, "Botão Direito Pressionado!"); break;
				}
			}
		} );

		
		this.add(jTable);
		this.setVisible(true);
	}

	public FrmListUsuarios(Usuario usuario) {
		
		this.usuario = usuario;
		
		if (usuario.getNivel()=="2"){
			// carrega todas as opções
		}else{
			// carrega só as opções deste usuário
		}
		
		this.setTitle("Cadastro do Usuario");
		this.setBounds(250, 250, 600, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new FrmListUsuarios();
	}
}
