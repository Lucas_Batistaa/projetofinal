package View;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class FrmMainMenu  extends JFrame{
	
	private void centarlizar() {
		Dimension desktop = Toolkit.getDefaultToolkit().getScreenSize();
		int w = (int) desktop.getWidth();
		int h = (int) desktop.getHeight();
		int x =  (w/2) - ( this.getWidth() / 2);
		int y =  (h/2) - ( this.getHeight()/ 2);
		this.setLocation(x, y);
	}
	
	private void addMenu() {
		
		JMenuBar 	barraMenu 	  = new JMenuBar();
		JMenu	 	mnuSistema	  = new JMenu("Sistema");
		JMenu	 	mnuCadastros  = new JMenu("Cadastros");
		JMenuItem	mnuSobre  	  = new JMenuItem("Sobre");
		mnuSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, " Desenvolvido por: \n\t Prof� Cleber S. Oliveira");
			}
		});
		
		
		
		JMenuItem   mnuSair		  = new JMenuItem("Sair");
		mnuSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JMenuItem   mnuUsuarios		  = new JMenuItem("Usu�rios");
		mnuUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new FrmListUsuarios();
			}
		});
		
		
		mnuSistema.add(mnuSair);
		mnuCadastros.add(mnuUsuarios);
		
		barraMenu.add(mnuSistema);
		barraMenu.add(mnuCadastros);
		barraMenu.add(mnuSobre);
		barraMenu.add(new JMenuItem());
		
		this.setJMenuBar(barraMenu);
		
	}
	
	public FrmMainMenu() {
		this.setTitle("Menu Principal");
		this.setLayout(null);
		this.setSize(900, 700);
		this.centarlizar();		
		this.addMenu();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
}
